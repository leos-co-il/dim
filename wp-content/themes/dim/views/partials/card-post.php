<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-md-6 col-sm-10 col-12 mb-4 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-img <?= !has_post_thumbnail() ? 'not-img': ''; ?>"<?php if (has_post_thumbnail($args['post'])) : ?>
				style="background-image: url('<?= postThumb($args['post']); ?>')"
			<?php endif; ?> href="<?= $link; ?>">
				<span class="border-wrap post-link">
				</span>
			</a>
			<div class="post-card-content">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
			</div>
		</div>
	</div>
<?php endif; ?>
