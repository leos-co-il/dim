<div class="form-home-main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if (isset($args['title']) && $args['title']) : ?>
					<h2 class="base-title-white mb-2"><?= $args['title']; ?></h2>
				<?php endif;
				getForm('45'); ?>
			</div>
		</div>
	</div>
</div>
