<?php if(isset($args['review']) && $args['review']) : $review = $args['review']; ?>
	<div class="col-md-6 col-12 review-col">
		<div class="review-item more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
			<div class="rev-content">
				<?php if ($logo = opt('logo_white')) : ?>
					<div class="logo-rev">
						<img src="<?= $logo['url']; ?>" alt="logo">
					</div>
				<?php endif; ?>
				<h3 class="review-title"><?= $review['name']; ?></h3>
				<p class="base-text-white text-center">
					<?= text_preview($review['review_body'], '30'); ?>
				</p>
			</div>
		</div>
		<div class="editions-trigger">
			<?php if ($review['review_video']) : ?>
				<div class="border-wrap play-button" data-video="<?= getYoutubeId($review['review_video']); ?>">
					<img src="<?= ICONS ?>play-button.png" alt="play-video">
				</div>
			<?php endif;
			if (text_count($review['review_body']) >= text_preview($review['review_body'], '30')) : ?>
				<div class="rev-pop-trigger border-wrap">
					<img src="<?= ICONS ?>plus.png" alt="more-review">
					<div class="hidden-review">
						<div class="d-flex flex-column justify-content-start align-items-center">
							<?php if ($logo) : ?>
								<div class="logo-rev">
									<img src="<?= $logo['url']; ?>" alt="logo">
								</div>
							<?php endif; ?>
							<h3 class="review-title"><?= $review['name']; ?></h3>
							<div class="base-output text-center white-text">
								<?= $review['review_body']; ?>
							</div>
						</div>
						<?php if ($review['review_video']) : ?>
						<a class="border-wrap align-self-center" href="<?= getYoutubeId($review['review_video']); ?>" target="_blank">
							<img src="<?= ICONS ?>play-button.png" alt="play-video-inside">
						</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
