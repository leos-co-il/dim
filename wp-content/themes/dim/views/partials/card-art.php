<?php if(isset($args['art']) && $args['art']) : ?>
	<div class="col-md-6 col-12 review-col">
		<div class="art-item more-card" data-id="<?= isset($args['num']) && $args['num'] ? $args['num'] : '' ?>">
			<div class="art-img">
				<?php if ($args['art']['art_img']) : ?>
					<img src="<?= $args['art']['art_img']['url']; ?>" alt="logo">
				<?php else : ?>
					<img src="<?= IMG ?>logo-article.png" alt="logo">
				<?php endif; ?>
			</div>
			<div class="art-content">
				<h3 class="art-title"><?= $args['art']['art_title']; ?></h3>
				<p class="base-text-white">
					<?= $args['art']['art_text']; ?>
				</p>
				<?php if ($args['art']['art_link']) : ?>
					<a href="<?= $args['art']['art_link']['url']; ?>" class="border-wrap post-link"></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
