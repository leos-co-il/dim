<?php if (isset($args['faq']) && $args['faq']) :
	$title = lang_text(['he' => 'שאלתם ענינו', 'en' => 'You ask, we answer', 'ru' => 'Вопросы и ответы'], 'he'); ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-output text-center">
						<?= (isset($args['text']) && $args['text']) ? $args['text'] : '<h2>'.$title.'</h2>'; ?>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-body-title"><?= $item['faq_question']; ?></span>
										<span class="faq-arrow"></span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output slider-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
