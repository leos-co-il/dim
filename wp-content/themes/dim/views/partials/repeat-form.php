<div class="repeat-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-form-wrap">
					<?php if ($f_main_title = opt('base_form_title')) : ?>
						<h2 class="base-title-white mb-2"><?= $f_main_title; ?></h2>
					<?php endif;
					getForm('45'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
