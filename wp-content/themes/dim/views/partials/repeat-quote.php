<?php $text = (isset($args['quote']) && $args['quote']) ? $args['quote'] : opt('offer_text'); ?>
<div class="quote-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto mb-2 d-flex justify-content-center align-items-center">
				<img src="<?= ICONS ?>quote.png" alt="balance">
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-10 col-md-11 col-12">
				<div class="quote-item">
					<p class="quote-text"><?= $text; ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
