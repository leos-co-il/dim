<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($video = $fields['home_main_video']) : ?>
<div class="main-video-block">
	<video muted autoplay="autoplay" loop="loop">
		<source src="<?= $video['url']; ?>" type="video/mp4">
	</video>
	<?php get_template_part('views/partials/repeat', 'form_home', [
			'title' => $fields['main_form_title']
	]); ?>
</div>
<?php elseif (!$video && has_post_thumbnail()) : ?>
	<div class="main-image-block" style="background-image: url('<?= postThumb(); ?>')">
		<?php get_template_part('views/partials/repeat', 'form_home', [
				'title' => $fields['main_form_title'],
		]); ?>
	</div>
<?php endif;
if ($fields['h_services']) : ?>
	<div class="serv-link-block-wrap">
		<div class="services-block">
			<div class="services-line">
				<div class="container-fluid">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['h_services'] as $i => $service) :
							$img_back = get_field('serv_img', $service->ID); ?>
							<div class="col-xl-4 col-md-6 col-12 serv-col wow zoomIn" data-wow-delay="0.<?= $i * 2; ?>s">
								<a class="home-service" href="<?= get_permalink($service); ?>"
										<?php if ($image = $img_back ? $img_back['url'] : (has_post_thumbnail() ? postThumb() : '')) : ?>
											style="background-image: url('<?= $image; ?>')"
										<?php endif; ?>>
								<span class="home-service-title">
									<?= $service->post_title; ?>
								</span>
									<span class="home-serv-overlay">
									<span class="serv-cont-wrap">
										<span class="art-title"><?= $service->post_title; ?></span>
									<span class="base-text-white">
										<?= text_preview($service->post_content, 15); ?>
									</span>
									<span class="border-wrap post-link"></span>
									</span>
								</span>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($fields['h_services_link']) : ?>
			<div class="link-block-wrap">
				<a href="<?= $fields['h_services_link']['url']; ?>" class="block-link">
					<?php $about_link = lang_text(['he' => ' לכל השירותים שלנו', 'en' => 'To all services', 'ru' => 'Больше услуг'], 'he');
					echo (isset($fields['h_services_link']['title']) && $fields['h_services_link']['title']) ?
							$fields['h_services_link']['title'] : $about_link; ?>
				</a>
			</div>
		<?php endif; ?>
	</div>
<?php endif;
if ($fields['h_review_item'] && ($fields['h_review_item']['review_body']) || $fields['h_review_item']['review_video']) : ?>
	<section class="home-reviews-block">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php if ($fields['h_reviews_img']) : ?>
					<div class="col-lg-6 col-12 rev-img-col wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= $fields['h_reviews_img']['url']; ?>" alt="success">
					</div>
				<?php endif; ?>
				<div class="<?= $fields['h_reviews_img'] ? 'col-lg-6 col-12' : 'col-12'; ?> home-rev-col">
					<div class="review-item">
						<div class="rev-content">
							<?php if ($logo = opt('logo_white')) : ?>
								<div class="logo-rev">
									<img src="<?= $logo['url']; ?>" alt="logo">
								</div>
							<?php endif; ?>
							<h3 class="review-title"><?= $fields['h_review_item']['name']; ?></h3>
							<p class="base-text-white text-center">
								<?= text_preview($fields['h_review_item']['review_body'], '30'); ?>
							</p>
						</div>
					</div>
					<div class="d-flex justify-content-between align-items-stretch flex-wrap">
						<div class="editions-trigger">
							<?php if ($fields['h_review_item']['review_video']) : ?>
								<div class="border-wrap play-button" data-video="<?= getYoutubeId($fields['h_review_item']['review_video']); ?>">
									<img src="<?= ICONS ?>play-button.png" alt="play-video">
								</div>
							<?php endif;
							if (text_count($fields['h_review_item']['review_body']) >= text_preview($fields['h_review_item']['review_body'], '30')) : ?>
								<div class="rev-pop-trigger border-wrap">
									<img src="<?= ICONS ?>plus.png" alt="more-review">
									<div class="hidden-review">
										<div class="d-flex flex-column justify-content-start align-items-center">
											<?php if ($logo) : ?>
												<div class="logo-rev">
													<img src="<?= $logo['url']; ?>" alt="logo">
												</div>
											<?php endif; ?>
											<h3 class="review-title"><?= $fields['h_review_item']['name']; ?></h3>
											<div class="base-output text-center white-text">
												<?= $fields['h_review_item']['review_body']; ?>
											</div>
										</div>
										<?php if ($fields['h_review_item']['review_video']) : ?>
											<a class="border-wrap align-self-center" href="<?= getYoutubeId($fields['h_review_item']['review_video']); ?>" target="_blank">
												<img src="<?= ICONS ?>play-button.png" alt="play-video-inside">
											</a>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<?php if ($fields['h_reviews_link']) : ?>
							<a href="<?= $fields['h_reviews_link']['url']; ?>" class="more-link about-link m-0">
								<?php $about_link = lang_text(['he' => 'לכל ההמלצות', 'en' => 'More reviews', 'ru' => 'Больше отзывов'], 'he');
								echo (isset($fields['h_reviews_link']['title']) && $fields['h_reviews_link']['title']) ?
										$fields['h_reviews_link']['title'] : $about_link; ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="video-modal">
		<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
			 aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body" id="iframe-wrapper"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="form-wrapper-pop">
					<span type="button" class="close-form" data-dismiss="modal" aria-label="Close">
						<img src="<?= ICONS ?>close.png">
					</span>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif; ?>
<section class="home-form-block white-back-form">
	<?php get_template_part('views/partials/repeat', 'quote', [
		'quote' => $fields['h_offer'],
	]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-start align-items-center mb-2">
					<?php if ($logo = opt('logo')) : ?>
						<div class="col-xl-auto col-12 d-flex justify-content-start align-items-stretch">
							<div class="logo">
								<a href="/" class="logo">
									<img src="<?= $logo['url'] ?>" alt="dim-logo">
								</a>
							</div>
						</div>
					<?php endif;
					if ($fields['h_form_title']) : ?>
						<div class="col-auto">
							<h2 class="base-title text-center mb-0"><?= $fields['h_form_title']; ?></h2>
						</div>
					<?php endif;
					if ($fields['h_form_subtitle']) : ?>
						<div class="col-auto">
							<h3 class="foo-form-subtitle"><?= $fields['h_form_subtitle']; ?></h3>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-12">
				<div class="form-wrapper-inside pt-3">
					<?php getForm('46'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['h_why_us_item'] || $fields['h_why_us_img']) : ?>
	<div class="why-us-block">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<div class="<?= $fields['h_why_us_img'] ? 'col-lg-6 col-12' : 'col-12'; ?> py-5">
					<?php if ($fields['h_why_us_title']) : ?>
						<div class="row justify-content-lg-start justify-content-center">
							<div class="col-auto">
								<h2 class="base-title-white"><?= $fields['h_why_us_title']; ?></h2>
							</div>
						</div>
					<?php endif;
					if ($fields['h_why_us_item']) : ?>
					<div class="row row-why-us align-items-center justify-content-lg-start justify-content-center">
						<?php foreach ($fields['h_why_us_item'] as $num => $why_item) : ?>
							<div class="col-lg-12 col-11 step-item wow fadeInUp"
								 data-wow-delay="0.<?= $num; ?>s">
								<div class="row align-items-center row-why-item">
									<div class="col-sm-auto mb-sm-0 mb-3 number-circle-col">
										<div class="number-circle">
											<?php if ($why_item_icon = $why_item['why_icon']) : ?>
												<img src="<?= $why_item_icon['url']; ?>" alt="why-us-icon">
											<?php endif; ?>
										</div>
									</div>
									<div class="col">
										<div class="process-wrap">
											<h3 class="process-title">
												<?= $why_item['why_title']; ?>
											</h3>
											<div class="base-text-white process-text">
												<?= $why_item['why_text']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['h_why_us_img'] || $fields['h_why_us_link']) : ?>
					<div class="col-lg-6 col-12 why-img-col wow fadeInUp" data-wow-delay="0.2s">
						<?php if ($fields['h_why_us_img']) : ?>
							<img src="<?= $fields['h_why_us_img']['url']; ?>" alt="image">
						<?php endif;
						if ($fields['h_why_us_link']) : ?>
							<a href="<?= $fields['h_why_us_link']['url']; ?>" class="more-link about-link">
								<?php $about_link = lang_text(['he' => 'עוד אודותינו', 'en' => 'More about us', 'ru' => 'Больше о нас'], 'he');
								echo (isset($fields['h_why_us_link']['title']) && $fields['h_why_us_link']['title']) ?
										$fields['h_why_us_link']['title'] : $about_link; ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_posts_text'] || $fields['h_posts']) : ?>
	<section class="home-posts">
		<?php if ($fields['h_posts_text']) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-6 col-lg-8 col-md-10 col-12">
						<div class="base-output white-centered-output text-center">
							<?= $fields['h_posts_text']; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;
		if ($fields['h_posts']) : ?>
		<div class="posts-output">
			<?php foreach ($fields['h_posts'] as $i => $post) {
				get_template_part('views/partials/card', 'post', [
						'post' => $post,
				]); } ?>
		</div>
		<?php endif;
		if ($fields['h_posts_link']) : ?>
			<div class="link-block-wrap">
				<a href="<?= $fields['h_posts_link']['url']; ?>" class="block-link">
					<?php $about_link = lang_text(['he' => ' לכל המאמרים שלנו', 'en' => 'To blog', 'ru' => 'Больше статей'], 'he');
					echo (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
							$fields['h_posts_link']['title'] : $about_link; ?>
				</a>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
endif;
if ($fields['h_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider',
			[
				'content' => $fields['h_slider_seo'],
				'img' => $fields['h_slider_img'],
			]); ?>
	</div>
<?php endif;
get_footer(); ?>
