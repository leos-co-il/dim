<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
?>
<article class="page-body contact-page-body pb-4">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-5">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-xl-6 col-lg-8 col-md-10 col-12">
				<h1 class="base-title-white text-center"><?php the_title(); ?></h1>
				<div class="base-output white-centered-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<div class="<?= has_post_thumbnail() ? 'col-lg-6 col-md-10 col-11' : 'col-12'; ?> contact-col">
				<?php if ($tel) : ?>
					<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-tel.png">
						</div>
						<div class="contact-info">
							<span class="contact-type"><?= $tel; ?></span>
						</div>
					</a>
				<?php endif; ?>
				<?php if ($mail) : ?>
					<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-mail.png">
						</div>
						<div class="contact-info">
							<span class="contact-type"><?= $mail; ?></span>
						</div>
					</a>
				<?php endif;
				if ($address) : ?>
					<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
						<div class="contact-icon-wrap">
							<img src="<?= ICONS ?>contact-geo.png">
						</div>
						<div class="contact-info">
							<h3 class="contact-type"><?= $address; ?></h3>
						</div>
					</a>
				<?php endif; ?>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-lg-6 col-md-10 col-12 d-flex justify-content-center align-items-center">
					<img src="<?= postThumb(); ?>" class="w-100" alt="contact-us">
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_footer('contact'); ?>
