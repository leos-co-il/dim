<?php
/*
Template Name: עיתונות
*/

get_header();
$fields = get_fields();


?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-10 col-12">
				<h1 class="base-title-white text-center"><?php the_title(); ?></h1>
				<div class="base-output white-centered-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['art_item']) : $all_arts = count($fields['art_item']); ?>
		<div class="reviews-output">
			<div class="container">
				<div class="row justify-content-center align-items-stretch put-here-posts">
					<?php foreach ($fields['art_item'] as $i => $art) :  ?>
						<?php if ($i < 6) : get_template_part('views/partials/card', 'art', [
								'art' => $art,
								'num' => $i,
						]); endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php if ($all_arts > 6) : ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="more-link load-more-posts" data-type="art" data-page="<?= get_queried_object_id(); ?>">
							<?= lang_text(['he' => 'טען עוד מהעיתונות', 'en' => 'Load more articles', 'ru' => 'Показать больше статей'], 'he'); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;
	endif; ?>
</article>
<section class="repeat-block-slider">
	<?php get_template_part('views/partials/repeat', 'form');
	if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider',
			[
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
			]);
	}
	?>
</section>
<section class="repeat-block-faq">
	<?php get_template_part('views/partials/repeat', 'quote',
		[
			'quote' => $fields['offer_text'],
		]);
	if ($fields['faq_item']) :
		get_template_part('views/partials/content', 'faq',
			[
				'text' => $fields['faq_text'],
				'faq' => $fields['faq_item'],
			]);
	endif; ?>
</section>
<?php get_footer(); ?>
