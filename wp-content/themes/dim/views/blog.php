<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
	'posts_per_page' => 12,
	'post_type' => $type,
	'suppress_filters' => false,
]);
$published_posts = count(get_posts([
	'numberposts' => -1,
	'post_type' => $type,
]));
$arr_link = ($type === 'service') ? ['he' => 'עוד שירותים', 'en' => 'More services', 'ru' => 'Больше услуг'] : ['he' => 'עוד מאמרים', 'en' => 'More posts', 'ru' => 'Больше статей'];
?>

<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
	<div class="container-fluid pt-2 mb-4">
		<div class="row justify-content-center">
			<div class="col-12">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-10 col-12">
				<h1 class="base-title-white text-center"><?php the_title(); ?></h1>
				<div class="base-output white-centered-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts->have_posts()) : ?>
		<div class="posts-output put-here-posts">
			<?php foreach ($posts->posts as $i => $post) {
				get_template_part('views/partials/card', 'post', [
						'post' => $post,
				]); } ?>
		</div>
	<?php endif;
	if ($published_posts && $published_posts > 12) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="<?= $type; ?>">
						<?= lang_text($arr_link, 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="repeat-block-slider">
	<?php get_template_part('views/partials/repeat', 'form');
	if ($fields['single_slider_seo']) {
		get_template_part('views/partials/content', 'slider',
			[
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
			]);
	}
	?>
</section>
<section class="repeat-block-faq">
	<?php get_template_part('views/partials/repeat', 'quote',
			[
					'quote' => $fields['offer_text'],
			]);
	if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
	endif; ?>
</section>
<?php get_footer(); ?>
