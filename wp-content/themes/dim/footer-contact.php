<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');
?>
<footer class="contact">
	<div class="footer-main">
		<div class="contact-form-block pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="row justify-content-start align-items-end mb-4">
							<?php if ($logo = opt('logo_white')) : ?>
								<div class="col-xl-auto col-12 d-flex justify-content-start align-items-stretch">
									<div class="logo">
										<a href="/" class="logo">
											<img src="<?= $logo['url'] ?>" alt="dim-logo">
										</a>
									</div>
								</div>
							<?php endif;
							if ($f_main_title = opt('foo_form_title')) : ?>
								<div class="col-auto">
									<h2 class="base-title-white mb-0"><?= $f_main_title; ?></h2>
								</div>
							<?php endif;
							if ($f_subtitle = opt('foo_form_subtitle')) : ?>
								<div class="col-auto">
									<h3 class="foo-form-subtitle"><?= $f_subtitle; ?></h3>
								</div>
							<?php endif; ?>
						</div>
						<div class="form-wrapper-inside">
							<?php getForm('41'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a id="go-top">
			<span class="border-wrap">
				<img src="<?= ICONS ?>arrow-top-white.png">
			</span>
			<h4 class="to-top-text mt-2">
				<?= lang_text(['he' => 'למעלה', 'en' => 'To top', 'ru' => 'Вверх'], 'he')?>
			</h4>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-sm-between justify-content-center align-items-start">
				<div class="col-md-auto col-sm-6 col-10 foo-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'מפת אתר', 'en' => 'Site map', 'ru' => 'Карта сайта'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '2'); ?>
					</div>
				</div>
				<div class="col-md-auto col-10 foo-menu foo-links-menu">
					<h3 class="foo-title">
						<?php $foo_l_title_base = lang_text(['he' => 'מאמרים פופולריים', 'en' => 'Popular articles', 'ru' => 'Популярные статьи'], 'he');
						$foo_l_title = opt('foo_menu_links_title');
						echo $foo_l_title ? $foo_l_title : $foo_l_title_base; ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<div class="col-md-auto col-sm-6 col-10 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'צור קשר', 'en' => 'Contact us', 'ru' => 'Контакты'], 'he')?>
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= lang_text(['he' => 'כתובת:', 'en' => 'Address:', 'ru' => 'Адрес:'], 'he').$address; ?>
									</a>
								</li>
							<?php endif;
							if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'טלפון:', 'en' => 'Phone:', 'ru' => 'Телефон:'], 'he').$tel; ?>
									</a>
								</li>
							<?php endif;
							if ($fax) : ?>
								<li>
									<span class="contact-info-footer">
										<?= lang_text(['he' => 'פקס:', 'en' => 'Fax:', 'ru' => 'Факс:'], 'he').$fax; ?>
									</span>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'אימייל:', 'en' => 'Email:', 'ru' => 'Электронная почта:'], 'he').$mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
