<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 12,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$published_posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>
<article class="article-page-body page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-4">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-10 col-12">
				<h1 class="base-title-white text-center"><?= $query->name; ?></h1>
				<div class="base-output white-centered-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts->have_posts()) : ?>
		<div class="posts-output put-here-posts">
			<?php foreach ($posts->posts as $i => $post) {
				get_template_part('views/partials/card', 'post', [
						'post' => $post,
				]); } ?>
		</div>
	<?php endif;
	if ($published_posts && $published_posts > 12) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="post" data-term="<?= $query->term_id; ?>">
						<?= lang_text(['he' => 'עוד מאמרים', 'en' => 'More posts', 'ru' => 'Больше статей'], 'he'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<section class="repeat-block-slider">
	<?php get_template_part('views/partials/repeat', 'form');
	if ($slider = opt('single_slider_seo', $query)) {
		get_template_part('views/partials/content', 'slider',
				[
						'content' => $slider,
						'img' => get_field('slider_img', $query),
				]);
	}
	?>
</section>
<section class="repeat-block-faq">
	<?php
	get_template_part('views/partials/repeat', 'quote',
			[
					'quote' => get_field('offer_text', $query),
			]);
	if ($faq = get_field('faq_item', $query)) :
		get_template_part('views/partials/content', 'faq',
				[
						'text' => get_field('faq_text', $query),
						'faq' => $faq,
				]);
	endif; ?>
</section>
<?php get_footer(); ?>
