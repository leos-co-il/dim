<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="drop-menu">
		<nav class="header-nav">
			<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
		</nav>
	</div>
	<div class="container-fluid">
        <div class="row justify-content-between">
			<div class="col d-flex justify-content-start align-items-stretch header-col-line">
				<div class="border-wrap">
					<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
					</button>
				</div>
				<?php if ($tel = opt('tel')) : ?>
					<div class="border-wrap header-tel-wrap">
						<a href="tel:<?= $tel; ?>" class="header-tel">
							<span class="tel-number"><?= $tel; ?></span>
							<img src="<?= ICONS ?>header-tel.png" alt="tel">
						</a>
					</div>
				<?php endif; ?>
				<?php if (get_lang()) : ?>
					<div class="langs-wrap">
						<?php do_action( 'wpml_footer_language_selector'); ?>
					</div>
				<?php endif;
				if ($whatsapp = opt('whatsapp')) : ?>
					<div class="border-wrap link-hover whatsapp-fix ">
						<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="d-flex">
							<i class="fab fa-whatsapp"></i>
						</a>
					</div>
				<?php endif; ?>
				<div class="border-wrap pop-trigger">
					<img src="<?= ICONS ?>email.png" alt="pop-trigger">
				</div>
			</div>
			<?php if ($logo = opt('logo_header')) : ?>
				<div class="col-auto d-flex justify-content-end align-items-stretch">
					<div class="logo">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="dim-logo">
						</a>
					</div>
				</div>
			<?php endif; ?>
        </div>
    </div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9 col-12 d-flex justify-content-center">
				<div class="float-form d-flex flex-column align-items-center">
					<div class="form-wrapper-pop">
						<span class="close-form close-pop">
							<img src="<?= ICONS ?>close.png">
						</span>
						<?php if ($logo = opt('logo_white')) : ?>
							<div class="logo mb-3">
								<a href="/" class="logo">
									<img src="<?= $logo['url'] ?>" alt="dim-logo">
								</a>
							</div>
						<?php endif;
						if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('43'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
