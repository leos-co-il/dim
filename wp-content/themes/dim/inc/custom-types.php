<?php
if(CATALOG){
    function product_post_type() {

        $labels = array(
            'name'                => 'מוצרים',
            'singular_name'       => 'מוצרים',
            'menu_name'           => 'מוצרים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל המוצרים',
            'view_item'           => 'הצג מוצר',
            'add_new_item'        => 'הוסף מוצר חדש',
            'add_new'             => 'הוסף חדש',
            'edit_item'           => 'ערוך מוצר',
            'update_item'         => 'עדכון מוצר',
            'search_items'        => 'חפש מוצר',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'product',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'product',
            'description'         => 'מוצרים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'product_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-products',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'product', $args );

    }

    add_action( 'init', 'product_post_type', 0 );

    function product_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות מוצרים',
            'singular_name'              => 'קטגוריות מוצרים',
            'menu_name'                  => 'קטגוריות מוצרים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'product_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'product_cat', array( 'product' ), $args );

    }

    add_action( 'init', 'product_taxonomy', 0 );
}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}
if (SERVICES) {
	function service_post_type()
	{

		$labels = array(
			'name' => 'שירותים',
			'singular_name' => 'שירות',
			'menu_name' => 'שירותים',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל השירותים',
			'view_item' => 'הצג שירות',
			'add_new_item' => 'הוסף שירות חדש',
			'add_new' => 'הוסף חדש',
			'edit_item' => 'ערוך שירות',
			'update_item' => 'עדכון שירות',
			'search_items' => 'חפש שירות',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'service',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'service',
			'description' => 'שירותים',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('service_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-universal-access',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('service', $args);

	}

	add_action('init', 'service_post_type', 0);

	function service_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות שירותים',
			'singular_name' => 'קטגוריות שירותים',
			'menu_name' => 'קטגוריות שירותים',
			'all_items' => 'כל השירותים',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'service_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('service_cat', array('service'), $args);

	}

	add_action('init', 'service_taxonomy', 0);
}
