<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$currentType = get_post_type($postId);
$save = lang_text(['he' => 'הורידו את הקובץ', 'en' => 'Download file', 'ru' => 'Скачать файл'], 'he');
$video_title = lang_text(['he' => 'לצפות בסרט', 'en' => 'To watch a video', 'ru' => 'Смотреть видео'], 'he');
$post_gallery = $fields['post_gallery'];
?>
<article class="article-page-body">
	<?php if ( function_exists('yoast_breadcrumb')) : ?>
		<div class="container-fluid pt-2 mb-3">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="container pt-4">
		<div class="row justify-content-between align-items-stretch">
			<div class="col-lg-6 col-12 d-flex flex-column align-items-start">
				<div class="base-output page-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<?php if ($fields['post_video']) : ?>
					<div class="video-trigger-wrap mb-4">
						<span class="play-button" data-video="<?= getYoutubeId($fields['post_video']); ?>">
							<img src="<?=ICONS ?>play-video.png" alt="play-button">
						</span>
						<?= $fields['post_video_title'] ? $fields['post_video_title'] : $video_title; ?>
					</div>
					<div class="video-modal">
						<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
							 aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-body" id="iframe-wrapper"></div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="close-icon">
								<img src="<?= ICONS ?>close.png">
							</span>
									</button>
								</div>
							</div>
						</div>
					</div>
				<?php endif;
				if ($fields['post_file']) : ?>
					<a class="link-post-save" download href="<?= $fields['post_file']['url']; ?>">
						<span class="file-icon-wrap">
							<img src="<?= ICONS ?>pdf.png">
						</span>
						<span class="file-save-text">
							<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ?
									$fields['post_file']['title'] : $save; ?>
						</span>
					</a>
				<?php endif; ?>
				<div class="trigger-wrap">
					<a class="social-trigger">
						<span class="social-item">
							<img src="<?= ICONS ?>share-trigger.png" alt="share">
						</span>
						<span class="social-item-text">
							<?= opt('text_share'); ?>
						</span>
					</a>
					<div class="all-socials item-socials" id="show-socials">
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="socials-wrap social-item">
							<img src="<?= ICONS ?>share-whatsapp.png" alt="share-whatsapp">
						</a>
						<a class="socials-wrap social-item" href="https://www.facebook.com/sharer/sharer.php?u=#<?= $current_id; ?>"
						   target="_blank">
							<img src="<?= ICONS ?>share-facebook.png" alt="share-facebook">
						</a>
						<a class="socials-wrap social-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>share-mail.png" alt="share-mail">
						</a>
					</div>
				</div>
			</div>
			<div class="col-xl-5 col-lg-6 col-12 mt-md-0 mt-4">
				<div class="sticky-form">
					<div class="form-wrapper-pop">
						<?php if ($logo = opt('logo_white')) : ?>
							<div class="logo mb-3">
								<a href="/" class="logo">
									<img src="<?= $logo['url'] ?>" alt="dim-logo">
								</a>
							</div>
						<?php endif;
						if ($f_title = opt('post_form_title')) : ?>
							<h2 class="pop-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('44'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($post_gallery) : ?>
	<div class="black-back gallery-slider-block pb-5 mt-5">
		<div class="gallery-slider" dir="rtl">
			<?php if (has_post_thumbnail()) : ?>
				<div class="slider-wrapper">
					<a class="slider-picture" href="<?= postThumb(); ?>" data-lightbox="images"
					   style="background-image: url('<?= postThumb(); ?>')">
					</a>
				</div>
			<?php endif;
			foreach ($post_gallery as $img) : ?>
				<div class="slider-wrapper">
					<a class="slider-picture" data-lightbox="images"
					   style="background-image: url('<?= $img['url']; ?>')"></a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif;
$taxname = 'category';
$text = lang_text(['he' => 'מאמרים נוספים', 'en' => 'More articles', 'ru' => 'Похожие статьи'], 'he');
switch ($currentType) {
	case 'service':
		$taxname = 'service_cat';
		$text = lang_text(['he' => 'שירותים נוספים', 'en' => 'More services', 'ru' => 'Похожие услуги'], 'he');
		$sameTitle = '<h2>'.$text.'</h2>';
		break;
	case 'post':
		$taxname = 'category';
		$sameTitle = '<h2>'.$text.'</h2>';
		break;
	default:
		$taxname = 'category';
		$sameTitle = '<h2>'.$text.'</h2>';
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 4,
		'post_type' => $currentType,
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => $taxname,
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => $currentType,
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
<section class="home-posts">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-10 col-12">
				<?php if ($fields['same_text']) : ?>
				<div class="base-output white-centered-output text-center">
					<?= $fields['same_text']; ?>
				</div>
				<?php else: ?>
					<h2 class="base-title-white text-center">
						<?= $sameTitle; ?>
					</h2>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="posts-output">
		<?php foreach ($samePosts as $i => $post) {
			get_template_part('views/partials/card', 'post', [
				'post' => $post,
			]); } ?>
	</div>
</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
endif;
get_footer(); ?>
