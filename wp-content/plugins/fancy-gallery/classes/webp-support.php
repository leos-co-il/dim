<?php Namespace WordPress\Plugin\GalleryManager;

abstract class WebP_Support {

  public static function init(){
    add_Filter('upload_mimes', [static::class, 'filterAllowedMimeTypes']);
    add_Filter('wp_check_filetype_and_ext', [static::class, 'filterExtensions'], 10, 4);
    add_filter('file_is_displayable_image', [static::class, 'filterFileIsdisplayable'], 10, 2);
  }

  public static function filterAllowedMimeTypes($arr_mime_types){
    $arr_mime_types['webp'] = 'image/webp';
    return $arr_mime_types;
  }

  public static function filterExtensions($types, $file, $filename, $mimes){
    $ext = PathInfo($filename, PATHINFO_EXTENSION);
    if ($ext == 'webp'){
      $types['ext'] = $ext;
      $types['type'] = "image/{$ext}";
    }

    return $types;
  }

  public static function filterFileIsDisplayable($is_image, $file_path){
    $ext = PathInfo($file_path, PATHINFO_EXTENSION);
    if ($ext == 'webp'){
      $is_image = True;
    }

    return $is_image;
  }

}

WebP_Support::init();
